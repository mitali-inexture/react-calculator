//import React from 'react';
import React, { Component ,useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import registerServiceWorker from './registerServiceWorker';


// var value = "";

// function handleClick(e) {
//     calculate(e.target.name);
    
//   }

// function calculate(val) {
    
//      if(val == '='){
//         value = eval(value);
//      }else if(val == 'C'){
//         value = '';
//      } else{
//         value = value + val;
//      }
//      useEffect(() => localStorage.setItem('result', value)); 
//   }

// function Calculator(){
//     return (
//         <div className="button">
//             <button name="C" onClick={handleClick}>C</button><br/>
//             <button name="3" onClick={handleClick}>3</button>
//             <button name="2" onClick={handleClick}>2</button>
//             <button name="5" onClick={handleClick}>5</button>
//             <button name="+" onClick={handleClick}>+</button><br/>
//             <button name="1" onClick={handleClick}>1</button>
//             <button name="9" onClick={handleClick}>9</button>
//             <button name="6" onClick={handleClick}>6</button>
//             <button name="-" onClick={handleClick}>-</button><br/>
//             <button name="4" onClick={handleClick}>4</button>
//             <button name="8" onClick={handleClick}>8</button>
//             <button name="7" onClick={handleClick}>7</button>
//             <button name="*" onClick={handleClick}>x</button><br/>
//             <button name="." onClick={handleClick}>.</button>
//             <button name="0" onClick={handleClick}>0</button>
//             <button name="=" onClick={handleClick}>=</button>
//             <button name="/" onClick={handleClick}>÷</button><br/>
//         </div>   
//         );
// }

// function Result(){
//     console.log("hi");
//     const initialCount = +localStorage.getItem('result') || 0;
//     console.log(initialCount);
//     return (
//         <div className="result">
//             <p>{result}</p>
//         </div>
//     );
// }
function Calculator() {
    const [count, setCount] = useState(0);
   
    const handleIncrement = () =>
      setCount(currentCount => currentCount + 1);
   
    const handleDecrement = () =>
      setCount(currentCount => currentCount - 1);
   
    return (
      <div>
        <h1>{count}</h1>
   
        <button type="button" onClick={handleIncrement}>
          Increment
        </button>
        <button type="button" onClick={handleDecrement}>
          Decrement
        </button>
      </div>
    );
  };
ReactDOM.render(<Calculator/>, document.getElementById('root-result'));
// ReactDOM.render(<Calculator/>, document.getElementById('root'));
// registerServiceWorker();




 